//--------CONST DEFINITIONS---------//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "functions.h"

//-------FUNCTIONS DEFINITIONS-------//

//Function to initialize a list
List * initialize(){
    //Allocating memory for the list
    List * list = (List *)malloc(sizeof(List));

    //Initialization
    list->p_head = NULL;
    list->p_tail = NULL;

    return list;
}

//Getting the tail of a list
Element * getting_last_element(List * list){
    Element * element = list->p_head;
    Element * begin_element = element;

    while(element->p_next->p_next != NULL){
        element = element->p_next;
        if(element->s_data[0] == '\0') begin_element = element->p_next;
    }

    return begin_element;
}

//Function to add a string between two elements
void insert_between_two_elements(Element * E_before, char * str){
    Element * E_after = E_before->p_next;
    Element * p_precedent_element = E_before;

    //How many elements we have to create
    int nb_element_to_add = ceil(strlen(str)/(float)TAILLE_DATA);

    for(int counter = 0; counter < nb_element_to_add; counter ++){
        //Create new element
        Element * new_element = malloc(sizeof(Element));

        // Copy the data from the string, if the string ends before the size of the data of an element, we add the end of string character
        for(int i_digit = 0; i_digit < TAILLE_DATA; i_digit++){
            if((counter * TAILLE_DATA + i_digit)<strlen(str)){
                new_element->s_data[i_digit] = str[counter * TAILLE_DATA + i_digit];
            }
            else{
                new_element->s_data[i_digit] = '\0';
                break;
            }
        }

        new_element->s_data[TAILLE_DATA] = '\0';

        //If first element then it's head else the p_next of the previous element to this new one
        if(counter == 0){
            E_before->p_next = new_element;
        }
        else{
            p_precedent_element->p_next = new_element;
        }

        //Future precedent element is this new one
        p_precedent_element = new_element;
    }
    //Creating a null element to separate elements
    Element * separation_element = malloc(sizeof(Element));
    separation_element->p_next = E_after;
    separation_element->s_data[0] = '\0';

    //Adding it to the list
    p_precedent_element->p_next = separation_element;
}

//Function to add an element to an initialized empty list
void insert_empty_list(List * list, char * str){
    Element * p_precedent_element;
    Element * p_first_element;

    //How many elements we have to create
    int nb_element_to_add = ceil(strlen(str)/(float)TAILLE_DATA);
    for(int counter = 0; counter < nb_element_to_add; counter ++){
        //Create new element
        Element * new_element = malloc(sizeof(Element));

        //Set the address of the next element to NULL
        new_element->p_next = NULL;

        // Copy the data from the string, if the string ends before the size of the data of an element, we add the end of string character
        for(int i_digit = 0; i_digit < TAILLE_DATA; i_digit++){
            if((counter * TAILLE_DATA + i_digit)<strlen(str)){
                new_element->s_data[i_digit] = str[counter * TAILLE_DATA + i_digit];
            }
            else{
                new_element->s_data[i_digit] = '\0';
                break;
            }
            new_element->s_data[TAILLE_DATA] = '\0';
        }

        //If first element then it's head else the p_next of the previous element to this new one, same for the tail
        if(counter == 0){
            list->p_head = new_element;
            list->p_tail = new_element;
        }
        else{
            p_precedent_element->p_next = new_element;
        }

        //Future precedent element is this new one
        p_precedent_element = new_element;
    }
    //Creating a null element to separate elements
    Element * separation_element = malloc(sizeof(Element));
    separation_element->p_next = NULL;
    separation_element->s_data[0] = '\0';

    //Adding it to the list
    p_precedent_element->p_next = separation_element;
}

//Function to add an element at the beginning of a list
void insert_begining_list(List * list, char * str){
    //If there is no element then stops the function
    if(list->p_head == NULL) return;

    Element * p_precedent_element;
    Element * p_next_element = list->p_head;

    //How many elements we have to create
    int nb_element_to_add = ceil(strlen(str)/(float)TAILLE_DATA);

    for(int counter = 0; counter < nb_element_to_add; counter ++){
        //Create new element
        Element * new_element = malloc(sizeof(Element));

        // Copy the data from the string, if the string ends before the size of the data of an element, we add the end of string character
        for(int i_digit = 0; i_digit < TAILLE_DATA; i_digit++){
            if((counter * TAILLE_DATA + i_digit)<strlen(str)){
                new_element->s_data[i_digit] = str[counter * TAILLE_DATA + i_digit];
            }
            else{
                new_element->s_data[i_digit] = '\0';
                break;
            }
            new_element->s_data[TAILLE_DATA] = '\0';
        }

        //If first element then it's head else the p_next of the previous element to this new one
        if(counter == 0){
            list->p_head = new_element;
        }
        else{
            p_precedent_element->p_next = new_element;
        }

        //Future precedent element is this new one
        p_precedent_element = new_element;
    }

    //Creating a null element to separate elements
    Element * separation_element = malloc(sizeof(Element));
    separation_element->p_next = p_next_element;
    separation_element->s_data[0] = '\0';

    //Adding it to the list
    p_precedent_element->p_next = separation_element;
}

//Function to add an element at the end of a list
void insert_end_list(List * list, char * str){
    //If there is no element then stops the function
    if(list->p_head == NULL) return;

    Element * p_last_item = list->p_tail;

    //Going after the last element of the last string
    while(p_last_item->s_data[0] != '\0'){
        p_last_item = p_last_item->p_next;
    }

    printf("Adding the string : %s", str);
    insert_between_two_elements(p_last_item, str);

    list->p_tail = p_last_item->p_next;
}

//Function that displays the entire list
void display_all(List * list){
    //If empty list then we exit and print EMPTY LIST
    if(list->p_head == NULL){
        printf("EMPTY LIST\n");
        return;
    }

    printf("HEAD");
    Element * temp_element = list->p_head;
    printf(" -> %s", temp_element->s_data, temp_element->p_next);
    while(temp_element->p_next != NULL){
        temp_element = temp_element->p_next;
        if(temp_element->s_data[0] == '\0') printf(" -> _", temp_element->s_data, temp_element->p_next);
        else printf(" -> %s", temp_element->s_data, temp_element->p_next);
    }
    printf("\nTAIL -> %s\n\n", list->p_tail->s_data);
}

// Function to add an element after the p ranked one
int insert_after_position(List * list, char * str, int p){
    if(p < 0) return -1;
    Element * p_previous_number = list->p_head;
    Element * p_next_element;
    Element * p_precedent_element;
    int position_counter = 0, occurence_counter = 0;

    //If first element then inserting at the beginning
    if(p == 0){
        insert_begining_list(list, str);
        return 0;
    }
    // Now we find the address to the separation element after the p-ranked element
    while(position_counter != p){
        //If the position doesn't exist then return 0
        if(p_previous_number->p_next == NULL) return -1;
        if(occurence_counter != 0){
            //If we are not seeing the first element then we go to the next element
            p_previous_number = p_previous_number->p_next;
        }

        //If we are on a separation element, then we count + 1 string
        if(p_previous_number->s_data[0] == '\0'){
            position_counter++;
        }

        occurence_counter++;
    }
    //The next element is the p+1-ranked one
    p_next_element = p_previous_number->p_next;

    insert_between_two_elements(p_previous_number, str);

    //Correcting the tail if needed
    list->p_tail = getting_last_element(list);

    return 0;
}

// Function to remove the p-ranked element
int remove_element_position(List * list, int p){
    //If position under 0(+1) then exiting with error -1
    if(p < 1) return -1;

    Element * p_previous_number = list->p_head;
    Element * p_element;
    int position_counter = 0, occurence_counter = 0;

    if(p != 1){
        do{
            p_previous_number = p_previous_number->p_next;
            if(p_previous_number->s_data[0] == '\0') position_counter++;
        }while(p_previous_number->p_next != NULL && position_counter < p-1);

        if(p_previous_number->p_next == NULL){
            return -1;
        }
        else{
            p_element = p_previous_number->p_next;
        }
    }
    else{
        p_element = p_previous_number;
    }

    //Now we can remove the p-rank element
    while(p_element->s_data[0] != '\0'){
        Element * temp_element;
        temp_element = p_element->p_next;
        free(p_element);
        p_element = temp_element;
    }

    //Deleting the separation element
    Element * temp_element = p_element->p_next;
    free(p_element);
    p_element = temp_element;

    //Now we link the previous element to the next one, if head then we modify head
    // We can change the tail too to prevent from bugs
    if(p == 1){
        if(p_element == NULL){
            list->p_head = NULL;
            list->p_tail = NULL;
        }
        else{
            list->p_head = p_element;
            list->p_tail = getting_last_element(list);
        }

    }
    else{
        p_previous_number->p_next = p_element;
        list->p_tail = getting_last_element(list);
    }

    return 0;
}

//Function to compare the value of two strings
int compare_string_values(char * str1, char * str2){
    int diff = strlen(str1) - strlen(str2);

    // If the two strings aren't the same size, we don't have to calculate the difference between their values
    if(diff < 0){
        return 2;
    }
    else if(diff > 0){
        return 1;
    }
    else{
        int counter = 0;
        char * value1 = str1[counter];
        char * value2 = str2[counter];

        //Comparing char by char
        while(value1 != '\0'){
            diff = value1 - value2;

            if(diff < 0){
                return 2;
            }
            else if(diff > 0){
                return 1;
            }

            counter++;
            value1 = str1[counter];
            value2 = str2[counter];
        }
        //If no difference, they are equal
    }
    return 0;
}

//Function to sort the values in a List
int sort(List * list){
    //If empty list then return -1
    if(list->p_head == NULL) return -1;
    //If not then start sorting
    Element * temp = list->p_head;
    Element * p_first_element = temp;
    int sorted = 0;
    while(temp->p_next != NULL){
        Element * p_element_temp;
        char * value_1 = (char *) malloc(sizeof(char));
        char * value_2 = (char *) malloc(sizeof(char));
        char * s_temp;
        int counter = 1;

        //Init the strings
        value_1[0] = '\0';
        value_2[0] = '\0';

        //Element to study
        while(temp->s_data[0] != '\0'){
            s_temp = value_1;
            value_1 = (char *)malloc(((counter*TAILLE_DATA)+1)*sizeof(char));

            //copying temp into value_1
            strcpy(value_1, s_temp);

            //Adding the new string
            strcat(value_1, temp->s_data);
            free(s_temp);
            temp = temp->p_next;
            counter ++;
        }

        //If no element after then exiting
        if(temp->p_next != NULL){
                        //Next element
            p_element_temp = temp->p_next;

            while(p_element_temp->s_data[0] != '\0'){
                s_temp = value_2;
                value_2 = (char *)malloc(((counter*TAILLE_DATA)+1)*sizeof(char));

                //copying p_element_temp into value_2
                strcpy(value_2, s_temp);

                //Adding the new string
                strcat(value_2, p_element_temp->s_data);
                free(s_temp);
                if(p_element_temp->s_data[0] != '\0') p_element_temp = p_element_temp->p_next;
                counter ++;
            }

            // If str 2 < str 1 then we exchange their order
            if(compare_string_values(value_1, value_2) == 1){
                sorted = 1;
                //If first element then change head
                if(p_first_element == list->p_head){
                    printf("changing head\n");
                    // NEED TO ADD HEAD CHANGING
                    Element * element_after = p_element_temp->p_next;
                    Element * element_second = list->p_head;

                    list->p_head = temp->p_next;
                    temp->p_next = element_after;
                    p_element_temp->p_next = element_second;
                }
                else{
                    Element * element_after = p_element_temp->p_next;
                    Element * element_second = p_first_element->p_next;

                    p_first_element->p_next = temp->p_next;
                    temp->p_next = element_after;
                    p_element_temp->p_next = element_second;
                }
            }
            if(!sorted){
                //Preparing next loop
                p_first_element = temp;
                temp = temp->p_next;
            }
            else{
                break;
            }

        }
    }

    //Checking that the tail is still the right one, if not then correcting it
    list->p_tail=getting_last_element(list);

    //If we sorted then we resort to be sure that no other element has to be sorted
    if(sorted) sort(list);

    return 0;
}

//Function to destroy a list
void destruct_list(List ** list){
    Element * element_actual;
    List * list_temp = *list;
    element_actual = list_temp->p_head;

    //Destroying every element
    while(element_actual->p_next != NULL){
        Element * element_temp;
        element_temp = element_actual->p_next;
        free(element_actual);
        element_actual = element_temp;
    }
    //Destroying separation block at the end
    free(element_actual);

    //Destroying the list
    free(list_temp);
}

// Function to sum all the strings inserted in the list, and adding the result at then end
int add_all_strings(List * list){
    if(list->p_head == NULL) return -1;
    Element * p_element = list->p_head;
    int counter = 1;
    char *temp;
    char * str1 = (char *) malloc(sizeof(char));

    str1[0] = '\0';

    //First string
    while(p_element->s_data[0] != '\0'){
            temp = str1;
            str1 = (char *)malloc(((counter*TAILLE_DATA)+1)*sizeof(char));

            //copying temp into value_1
            strcpy(str1, temp);

            //Adding the new string
            strcat(str1, p_element->s_data);
            free(temp);
            p_element = p_element->p_next;
            counter++;
    }

    //Then doing the others
    while(p_element->p_next != NULL){
        char * str2 = (char *) malloc(sizeof(char));
        str2[0] = '\0';

        counter = 1;
        p_element = p_element->p_next;
        while(p_element->s_data[0] != '\0'){
            temp = str2;
            str2 = (char *)malloc(((counter*TAILLE_DATA)+1)*sizeof(char));

            //copying temp into value_1
            strcpy(str2, temp);

            //Adding the new string
            strcat(str2, p_element->s_data);
            free(temp);
            p_element = p_element->p_next;
            counter++;
        }

        temp = addition_two_strings(str1, str2);
        free(str1);
        free(str2);
        str1 = temp;
    }
    insert_end_list(list, str1);

    return 0;
}

// Function to add an element after the p ranked one
char * addition_two_strings(char * str1, char * str2){
    int value, carry = 0, counter = 1, lenght = 0;
    char * result, *temp;
    //Taking the biggest strlen to create the result string
    if(strlen(str1) < strlen(str2)){
        result = malloc((strlen(str2)+2) * sizeof(char));
        lenght = strlen(str2) + 2;
    }
    else{
        result = malloc((strlen(str1)+2) * sizeof(char));
        lenght = strlen(str1) + 2;
    }

    while(counter <= strlen(str1) || counter <= strlen(str2)){
        //Adding the carry to the value
        value = carry;

        //Adding the next digit to value of string 1
        if(counter <= strlen(str1)){
            value += str1[strlen(str1) - counter] - 48;
        }
        // Same for string 2
        if(counter <= strlen(str2)){
            value += str2[strlen(str2) - counter] - 48;
        }

        //if more than 2 digits then we add it to the carry
        carry = floor(value/10);
        value = value - floor(value/10)*10;
        result[counter-1] = value + 48;
        counter++;
    }

    if(carry != 0){
        result[counter-1] = carry + 48;
        result[counter] = '\0';
    }
    else{
        result[counter-1] = '\0';
    }

    //Now returning the string
    temp = (char*)malloc(strlen(result)+1 * sizeof(char));
    for(counter = 0; counter < strlen(result); counter++){
        temp[counter] = result[(strlen(result) - 1) - counter];
    }
    temp[counter] = '\0';
    free(result);

    return temp;
}

//Function to make a demo of every function one by one
void all_demo(){
    List * my_list = initialize();
    printf("INSERTION WHEN LIST HAS NO ELEMENT\n");
    insert_empty_list(my_list, "1234567891011");
    printf("string to insert : 1234567891011\n");
    display_all(my_list);

    printf("\nADDING ANOTHER ONE AT THE BEGINNING\n");
    insert_begining_list(my_list, "5891");
    printf("string to insert : 5891\n");
    display_all(my_list);

    printf("\nADDING ANOTHER ONE AT THE END\n");
    insert_end_list(my_list, "22");
    printf("string to insert : 22\n");
    display_all(my_list);

    printf("\nADDING ANOTHER ONE AFTER THE 2nd POSITION\n");
    insert_after_position(my_list, "333", 2);
    printf("string to insert : 333\n");
    display_all(my_list);

    printf("\nREMOVING ELEMENT AT POSITION 4\n");
    remove_element_position(my_list, 4);
    display_all(my_list);

    printf("\nCOMPARING 35656 and 35657\n");
    printf("Biggest value : %d\n\n", compare_string_values("35656", "35657"));

    printf("\nCOMPARING 159888888888 and 159888888888\n");
    printf("Biggest value : %d\n\n", compare_string_values("159888888888", "159888888888"));

    printf("\nSORTING THE LIST\n");
    sort(my_list);
    display_all(my_list);

    printf("\nDESTROYING THE LIST\n");
    destruct_list(&my_list);
}

//function to edit, create, remove and manage a list
int menu(List * my_list){
    //If the user wants to exit then returns 1
    int choice;
    char to_insert[255];

    printf("MAIN MENU\n");
    printf("1 - Adding a number at the beginning of the list\n");
    printf("2 - Adding a number to the list at a precise rank\n");
    printf("3 - Adding a number at the end of the list\n");
    printf("4 - Removing a number in the list\n");
    printf("5 - Sorting the list\n");
    printf("6 - Summing all the values of the list\n");
    printf("7 - Showing the list\n");
    printf("0 - Deleting and exiting the program\n");
    printf("Your choice : \n");
    scanf("%d", &choice);

    //If the user wants to exit then we can exit the menu directly
    if(choice == 0) return 1;
    else if(choice == 7) return 0;

    //If the list is empty and he wants to sort it

    //If empty list then adding the first element, if another action then don't do anything
    if(my_list->p_head == NULL){
        if(choice == 1 || choice == 3){
            printf("Number to add : \n");
            scanf("%s", &to_insert);
            insert_empty_list(my_list, to_insert);
        }
        else if(choice == 2){
            printf("Empty list, start by adding an element to the list : \n");
            scanf("%s", &to_insert);
            insert_empty_list(my_list, to_insert);
        }
        else{
            printf("Empty list, can't do that.\n");
        }
    }
    else{
        int pos, res;
        switch(choice){
            case 1:
                printf("Number to add at the beginning of the list : \n");
                scanf("%s", &to_insert);
                insert_begining_list(my_list, to_insert);
                break;
            case 3:
                printf("Number to add at the end of the list : \n");
                scanf("%s", &to_insert);
                insert_end_list(my_list, to_insert);
                break;
            case 2:
                printf("Number to add : \n");
                scanf("%s", &to_insert);
                printf("Position : \n");
                scanf("%d", &pos);
                res = insert_after_position(my_list, to_insert, pos-1);
                if(res != 0) printf("\nWrong position\n");
                break;
            case 4:
                printf("Position of the number to remove : \n");
                scanf("%d", &pos);
                res = remove_element_position(my_list, pos);
                if(res != 0) printf("\nWrong position\n\n");
                break;
            case 5:
                sort(my_list);
                break;
            case 6:
                add_all_strings(my_list);
            default:
                return 0;
        }
    }

    return 0;
}
