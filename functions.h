#define TAILLE_DATA 5

//--------TYPEDEF DEFINITIONS---------//

typedef struct Element Element;
typedef struct List List;

//-------STRUCTURES DEFINITIONS-------//

struct Element{
    char s_data[TAILLE_DATA+1];
    struct Element * p_next;
};

struct List{
    struct Element * p_head;
    struct Element * p_tail;
};
//-------FUNCTIONS DEFINITIONS-------//

//Function to initialize a list
List * initialize();
//Getting the tail of a list
Element * getting_last_element(List *);
//Function to add a string between two elements
void insert_between_two_elements(Element *, char *);
//Function to add an element to an initialized empty list
void insert_empty_list(List *, char *);
//Function to add an element at the beginning of a list
void insert_begining_list(List *, char *);
//Function to add an element at the end of a list
void insert_end_list(List *, char *);
//Function that displays the entire list
void display_all(List *);
// Function to add an element after the p ranked one
int insert_after_position(List *, char *, int);
//Function to remove an p-ranked element
int remove_element_position(List *, int);
//Function to compare the value of two strings
int compare_string_values(char *, char *);
//Function to sort the values in a List
int sort(List *);
//Function to add an element after the p ranked one
char * addition_two_strings(char *, char *);
//Function to sum all the strings inserted in the list, and adding the result at then end
int add_all_strings(List *);
//Function to destroy a list
void destruct_list(List **);
//Function to make a demo of every function one by one
void all_demo();
//function to edit, create, remove and manage a list
int menu(List *);
