#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "functions.h"

//-------MAIN FUNCTION-------//

int main(){
    List * my_list = initialize();

    while(!menu(my_list)) display_all(my_list);

    printf("\nDestroying the list\n");
    destruct_list(&my_list);

    return 0;
}
